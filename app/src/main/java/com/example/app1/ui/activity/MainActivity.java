package com.example.app1.ui.activity;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.app1.R;
import com.example.app1.ui.fragment.TestFragment;

public class MainActivity extends AppCompatActivity implements TestFragment.OnFragmentInteractionListener, TestFragment1.OnFragmentInteractionListener2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("13", "onCreate");
        setContentView(R.layout.activity_main);


//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//
//        Fragment fraggy = new TestFragment();
//        Fragment fraggy1 = new TestFragment1();
//        fragmentTransaction.add(R.id.fTest, fraggy);
//        fragmentTransaction.add(R.id.fTest1, fraggy1);
//        fragmentTransaction.commit();

//        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fTest);
//        Bundle bundle = new Bundle();
//        bundle.putString("name", "John");
//        bundle.putString("email", "john@gmail.com");
//        bundle.putString("dob", "20-Jun-1990");
//        fragment.setArguments(bundle);


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("13", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("13", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("13", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("13", "onDestroy");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.e("13", "onBackPressed");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("13", "onRestart");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentInteraction2(Uri uri) {

    }
}
